let coursesData = [
    {
        id: "wdc001",
        name: "PHP-Laravel",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis vel placeat quam ipsa quae maxime dolorem obcaecati veniam, necessitatibus numquam nisi dolorum! Commodi voluptate totam distinctio. Reiciendis repellendus ratione voluptates.",
        price: 25000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python-Django",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis vel placeat quam ipsa quae maxime dolorem obcaecati veniam, necessitatibus numquam nisi dolorum! Commodi voluptate totam distinctio. Reiciendis repellendus ratione voluptates.",
        price: 35000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java-Springboot",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis vel placeat quam ipsa quae maxime dolorem obcaecati veniam, necessitatibus numquam nisi dolorum! Commodi voluptate totam distinctio. Reiciendis repellendus ratione voluptates.",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc004",
        name: "NodeJS-ExpressJS",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis vel placeat quam ipsa quae maxime dolorem obcaecati veniam, necessitatibus numquam nisi dolorum! Commodi voluptate totam distinctio. Reiciendis repellendus ratione voluptates.",
        price: 55000,
        onOffer: false
    }
]

export default coursesData
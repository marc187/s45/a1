import { Card, Button, Row, Col } from 'react-bootstrap';

export default function Highlights(){
    return(
        <>
            <Row className='m-5'>
                <Col xs={12} md={4}>
                    <Card style={{ width: '18rem' }} className='mx-auto'>
                      <Card.Body>
                        <Card.Title>Learn From Home</Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis vel placeat quam ipsa quae maxime dolorem obcaecati veniam, necessitatibus numquam nisi dolorum! Commodi voluptate totam distinctio. Reiciendis repellendus ratione voluptates.
                        </Card.Text>
                        
                      </Card.Body>
                    </Card>            
                </Col>
                <Col xs={12} md={4}>
                    <Card style={{ width: '18rem' }} className='mx-auto'>
                      <Card.Body>
                        <Card.Title>Be part of our community</Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis vel placeat quam ipsa quae maxime dolorem obcaecati veniam, necessitatibus numquam nisi dolorum! Commodi voluptate totam distinctio. Reiciendis repellendus ratione voluptates.
                        </Card.Text>
                        
                      </Card.Body>
                    </Card>            
                </Col>
                <Col xs={12} md={4}>
                    <Card style={{ width: '18rem' }} className='mx-auto'>
                      <Card.Body>
                        <Card.Title>Study Now Pay Later</Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis vel placeat quam ipsa quae maxime dolorem obcaecati veniam, necessitatibus numquam nisi dolorum! Commodi voluptate totam distinctio. Reiciendis repellendus ratione voluptates.
                        </Card.Text>
                        
                      </Card.Body>
                    </Card>            
                </Col>
            </Row>
        </>
        
    )
}
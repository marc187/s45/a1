
export default function Footer(){
    return(
        <div className="bg-info d-flex justify-content-center align-items-center text-white" style={{height: '10vh', marginTop: '50px'}}>
          <p className="m-0 font-weight-bold">Marc Allen Nanong &#64; Course Booking | Zuitt Coding Bootcamp &#169;</p>
        </div>
    )
}
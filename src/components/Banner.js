
export default function Banner(){
    return(
        <div className="jumbotron jumbotron-fluid">
            <div className="container">
                <h1 className="display-4">Welcome to Course Booking App</h1>
                <p className="lead">Opportunities for everyone, everywhere.</p>
                <a className='btn btn-info' href='#'>Click Here</a>
            </div>
        </div>        
    )
}
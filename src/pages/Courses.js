import CourseCard from "../components/CourseCard"
import coursesData from "../mockData/courses"

export default function Courses(){
    // console.log(coursesData[0])

    const courses =  coursesData.map(course => {
        return <CourseCard key={course.id} courseProp={course}/>
    })

    return(
        <>
            {courses}
        </>
    )
}
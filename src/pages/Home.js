import AppNavBar from "../components/AppNavBar";
import Banner from "../components/Banner";
import Footer from "../components/Footer";
import Highlights from "../components/Highlights";


export default function Home(){
    return(
        <>
            <AppNavBar/>
            <Banner/>
            <Highlights/>
            <Footer/>
        </>
    )
}
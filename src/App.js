import { Fragment } from "react";

import Home from "./pages/Home";
import Courses from "./pages/Courses";


function App() {
  return(
    <Fragment>
      {/* <Home/> */}
      <Courses/>
    </Fragment>    
  )
}

export default App;

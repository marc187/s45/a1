import React, { Fragment } from 'react';
import ReactDOM, { render } from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';


import App from './App';


// ReactDOM.render(what to render, where to render);

ReactDOM.render(
  <Fragment>
    <App/>
  </Fragment>
  ,
  document.getElementById(`root`)
);
